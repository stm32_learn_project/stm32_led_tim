#include "stm32f10x.h"                  // Device header
#include "RTE_Components.h"             // Component selection
#include "RTE_Device.h"                 // Keil::Device:Startup
#include "stm32f10x_exti.h"             // Keil::Device:StdPeriph Drivers:EXTI
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f10x_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM


void gpio_init(){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	GPIO_InitTypeDef led_configuration;
	led_configuration.GPIO_Pin=GPIO_Pin_2;
	led_configuration.GPIO_Mode=GPIO_Mode_Out_PP;
	led_configuration.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&led_configuration);
	
	GPIO_SetBits(GPIOB,GPIO_Pin_2);

}

void exti_init(){
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	
// 从0开始一直增加到999，1000个数触发一次中断 每隔1ms产生一次中断
	TIM_TimeBaseInitTypeDef tim_configuration;

	tim_configuration.TIM_CounterMode=TIM_CounterMode_Up; //向上计数
	tim_configuration.TIM_Period=999; //999+1=1000  1000*1=1ms
	tim_configuration.TIM_Prescaler=71; //71+1=72 分频  72Hz/72=1us

	TIM_TimeBaseInit(TIM2,&tim_configuration);
	
	TIM_ClearFlag(TIM2,TIM_FLAG_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM2,ENABLE);
	
	
}
void nvic_init(){
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1); 
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn; //通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2; 
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure); 
}
u32 time_count=0;
void TIM2_IRQHandler(){
	if (TIM_GetITStatus(TIM2,TIM_IT_Update)==1)
		{
			time_count++;
			if (time_count>=500)
				{				
					time_count=0;
					if (GPIO_ReadOutputDataBit(GPIOB,GPIO_Pin_2)){
						GPIO_ResetBits(GPIOB,GPIO_Pin_2);
					}else{
						GPIO_SetBits(GPIOB,GPIO_Pin_2);
					}
					
					
		}
		TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
	}

}


int main(void){
	gpio_init();
	nvic_init();
	exti_init();
	while(1){
	}
		
	
	
}

